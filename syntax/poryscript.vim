" Vim syntax file
" Language:		Poryscript
" Maintainer:		leo vriska <leo@60228.dev>
" Last Change:		2024 Jun 1

" Quit when a (custom) syntax file was already loaded
if exists("b:current_syntax")
  finish
endif

let s:cpo_save = &cpo
set cpo&vim

syn include	@poryscriptAsm		syntax/asm.vim

syn keyword	poryscriptStatement	mart break continue const
syn keyword	poryscriptStatement	raw nextgroup=poryscriptRaw skipwhite skipnl
syn keyword	poryscriptStatement	script text movement mapscripts nextgroup=poryscriptScope
syn keyword	poryscriptOperator	format var flag defeated value
syn keyword	poryscriptConstant	TRUE FALSE true false
syn keyword	poryscriptConditional	if else elif switch poryswitch
syn keyword	poryscriptRepeat	do while
syn keyword	poryscriptLabel		case default

syn keyword	poryscriptScopeGlobal	global contained
syn keyword	poryscriptScopeLocal	local contained
syn match	poryscriptScopeDelim	/[()]/ contained
syn match	poryscriptScope		/([^()]*)/ contained contains=poryscriptScopeGlobal,poryscriptScopeLocal transparent

syn match	poryscriptSpecial	display contained "\\[lnpN\\"]"
syn region	poryscriptControlCode	start=/{/ end=/}\|$/ contains=poryscriptControlCode contained
syn region	poryscriptString	start=+\([a-zA-Z]\+[a-zA-Z0-9]*\)\="+ skip=+\\\\\|\\"+ keepend excludenl end=+"+ end='$' contains=poryscriptSpecial,poryscriptControlCode,@Spell

syn region	poryscriptRaw		start="`" end="`" contains=@poryscriptAsm contained

syn match	poryscriptNumber	display "\<0x\x\+\>\|\<-\=\d\+\>"

syn region	poryscriptComment	start="//\|#" end="$" keepend contains=@Spell

" Define the default highlighting.
" Only used when an item doesn't have highlighting yet
hi def link	poryscriptStatement	Statement
hi def link	poryscriptOperator	Operator
hi def link	poryscriptConstant	Constant
hi def link	poryscriptConditional	Conditional
hi def link	poryscriptRepeat	Repeat
hi def link	poryscriptLabel		Label
hi def link	poryscriptScopeDelim	Delimiter
hi def link	poryscriptScopeGlobal	Keyword
hi def link	poryscriptScopeLocal	Keyword
hi def link	poryscriptSpecial	SpecialChar
hi def link	poryscriptControlCode	poryscriptSpecial
hi def link	poryscriptString	String
hi def link	poryscriptNumber	Number
hi def link	poryscriptComment	Comment

let b:current_syntax = "poryscript"

let &cpo = s:cpo_save
unlet s:cpo_save
" vim: ts=8
