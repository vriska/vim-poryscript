" Vim indent file
" Language:		Poryscript
" Maintainer:		leo vriska <leo@60228.dev>
" Last Change:		2024 Jun 1

" Only load this indent file when no other was loaded.
if exists("b:did_indent")
   finish
endif
let b:did_indent = 1

setlocal cindent
" L0 -> 0 indent for jump labels
" +0 -> 0 indent for continuation lines (no semicolons)
" J1 -> "javascript object declarations" (colons for map entries, not labels)
" #1 -> # for comments
" (0 -> align contents of parentheses to same start column
setlocal cinoptions+=L0,+0,J1,#1,(0

setlocal indentexpr=GetPoryscriptIndent()
setlocal indentkeys=0{,0},0),0],<:>,`,!^F,o,O,e

let b:undo_indent = "setlocal cindent< cinoptions< indentexpr< indentkeys<"

function! GetPoryscriptIndent()
   let prevLineNum = prevnonblank(v:lnum - 1)
   let prevLine = getline(prevLineNum)

   if has('syntax_items')
      let stack = synstack(v:lnum, 1)
      if len(stack) > 0 && synIDattr(stack[0], "name") ==# "poryscriptRaw"
         if getline(v:lnum) =~ '^\s*\k\+:\|\s*`'
            return 0
         endif
         return shiftwidth()
      endif
   endif

   if prevLine =~ '\v[$'
      if getline(v:lnum) =~ '\v^\s*]$'
         return indent(prevLineNum)
      endif
      return indent(prevLineNum) + shiftwidth()
   elseif getline(v:lnum) =~ '\v^\s*]'
      return indent(prevLineNum) - shiftwidth()
   elseif getline(v:lnum) =~ '\v^\s*#'
      return indent(prevLineNum)
   endif

   return cindent(v:lnum)
endfunction
" vim: ts=8 expandtab
