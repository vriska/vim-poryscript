# vim-poryscript

This Vim plugin provides [Poryscript](https://github.com/huderlem/poryscript) file detection, syntax highlighting, and indentation.
